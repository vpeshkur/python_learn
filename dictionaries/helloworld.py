
# При запуске скрипта как отдельного модуля переменной __name__ присваивается значение __main__
if __name__ == "__main__":
    print(f"Run as independent programm __name__ is {__name__}")
else:
    print(f"Run as import module")