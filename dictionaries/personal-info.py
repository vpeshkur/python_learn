
name_and_age = {} #create dictionary

for x in range(3):
    firstName = input("What is your name? ")
    age = input ("What is your age? ")
    name_and_age[firstName] = age


firstName_output = input("Entering name ")
print("Hi, " + firstName_output + ". Your age is " + name_and_age.get(firstName_output, "Absent in dictionary :-(") + ".")

print("Hi, " + firstName_output + ". Your age is " + name_and_age[firstName_output] + ".")